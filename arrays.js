//  1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift"
//  та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.
//

const wordsList = ["travel", "hello", "eat", "ski", "lift",];
function countSymbols(arr) {
    return arr.filter((element) => element.length > 3).length;
}
console.log(countSymbols(wordsList));


//  2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}.
//  Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
//  Відфільтрований масив виведіть в консоль.
//

const peopleList = [
    {name: "Іван", age: 25, sex: "чоловіча"},
    {name: "Максим", age: 30, sex: "чоловіча"},
    {name: "Оксана", age: 29, sex: "жіноча"},
    {name: "Артем", age: 45, sex: "жіноча"}
];
function filterPersonBySex(arr) {
    return arr.filter((element) => element.sex === 'чоловіча');
};
console.log(filterPersonBySex(peopleList));

//  3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
//
// Технічні вимоги:
//
// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив,
// який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент,
// за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив
// ['hello', 'world', 23, '23', null], і другим аргументом передати 'string',
// то функція поверне масив [23, null].


const someArray = ['hello', 'world', 23, '23', null, {name: 'John', age: '25'}, 45]
function filterBy(arr, type) {
    return arr.filter((element) => typeof element !== type);
}
console.log(filterBy(someArray, 'string'));